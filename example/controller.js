function Populate () {
    var StringModel = new Ces().Model('StringModel', {
        "String1":"Joe", 
        "String2":"Bloggs"
    });
    var ArrayModel = new Ces().Model('ArrayModel', {
        "Array1": [
            "Wales",
            "England",
            "Scotland",
            "Ireland"
        ],
        "Array2": [
            "Wales"
        ]
    });
    var MixedModel = new Ces().Model('MixedModel', {
        "Date": new Date(),
        "String": "bg-black"
    });

    Ces('#Firstname').text(StringModel.String1);
    Ces('#Lastname').text(StringModel.String2);
    Ces('#DOB').attribute('value', MixedModel.Date.getFullYear() + '-' + '0' + String(parseInt(MixedModel.Date.getMonth()) + 1).slice(-2) + '-' + MixedModel.Date.getDate());
    for (var i = 0; i < ArrayModel.Array1.length; i++) {
        Ces('#Country').append(Ces().new('<option value="' + ArrayModel.Array1[i] + '">' + ArrayModel.Array1[i] + '</option>'));
    }
    Ces('#Country option[value="' + ArrayModel.Array2[0] + '"]').attribute('selected', true);
    Ces('.bg-white').class(MixedModel.String, true);
    Ces('.bg-white').class('bg-white', false);
}
document.ready(function (){
    Ces('#Populate').on('click', function () {
        Populate();
    });
});