Ces().DefineModels([
    {
        "name":"StringModel",
        "properties":
        {
            "String1":"string", 
            "String2":"string"
        }
    },
    {
        "name":"ArrayModel",
        "properties":
        {
            "Array1":"array", 
            "Array2":"array"
        }
    },
    {
        "name":"MixedModel",
        "properties":
        {
            "Date":"date", 
            "String":"string"
        }
    }
]);