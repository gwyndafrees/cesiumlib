// Set version
v = "0.01";

/* Cesium object initialisation */

// Initialises new Cesium object when Ces() is called
Ces = function(selector) {
    if(arguments.length < 1 || typeof selector === 'undefined')
        return new Ces.init.fn();
    else if (typeof selector === 'string' && selector !== '')
        return new Ces.init.get(selector);
    else 
        return new Ces.init.fn();
}

Models = [];

window.Ces.init = (function () {
    
    function Ces (el) {
        // Ces object length property - will always have length
        if(typeof el !== 'undefined' && el !== null) {
            for(var i = 0; i < el.length; i++) {
                this[i] = el[i];
            }
            this.length = el.length;
        }
        else {
            this.length = 0;
        }
        // Define version
        if(this.length === 0) {
            this.CesVersion = v;
            this.Models = ces.models();
        }
    }
    /* Cesium object functions */

    // Map function for Cesium object - loops over the Cesium object and applies a function to each of the elements
    Ces.prototype.map = function (fn) {
        var arr = [];
        for(var i = 0;i < this.length;i++) {
            arr.push(fn.call(this, this[i], i));
        }
        return arr;
    }

    // For each for Cesium object
    Ces.prototype.each = function (fn) {
        this.map(fn);
        return this;
    }

    // Map single either returns the array or the value within the array if there is only one element
    Ces.prototype.mapSingle = function (fn) {
        var res = this.map(fn);
        return res.length > 1 ? res : res[0];
    };

    Ces.prototype.on = (function () {
        if (document.addEventListener) {
            return function (evt, fn) {
                return Array.from(this).forEach(function (el) {
                    el.addEventListener(evt, fn, false);
                });
            };
        } else if (document.attachEvent)  {
            return function (evt, fn) {
                return Array.from(this).forEach(function (el) {
                    el.attachEvent("on" + evt, fn);
                });
            };
        } else {
            return function (evt, fn) {
                return Array.from(this).forEach(function (el) {
                    el["on" + evt] = fn;
                });
            };
        }
    }());

    Ces.prototype.off = (function () {
        if (document.removeEventListener) {
            return function (evt, fn) {
                return Array.from(this).forEach(function (el) {
                    el.removeEventListener(evt, fn, false);
                });
            };
        } else if (document.detachEvent)  {
            return function (evt, fn) {
                return Array.from(this).forEach(function (el) {
                    el.detachEvent("on" + evt, fn);
                });
            };
        } else {
            return function (evt, fn) {
                return Array.from(this).forEach(function (el) {
                    el["on" + evt] = null;
                });
            };
        }
    }());

    // Sets the text of an element or gets the text value of it if no string parameter is specified
    Ces.prototype.text = function (str) {
        if (typeof str !== 'undefined'){
            return Array.from(this).forEach(function (el) { 
                if(el instanceof HTMLInputElement && el.type === 'text') // If textbox, use value
                    el.value = str;
                else
                    el.innerText = str;
            });
        }
        else {
            return this.mapSingle(function (el) {
                return el.innerText;
            });
        }
    }

    // Sets the html of an element or gets the html value of it if no string parameter is specified
    Ces.prototype.html = function (str) {
        if (typeof str !== 'undefined'){
            return Array.from(this).forEach(function (el) {
                el.innerHTML = str;
            });
        }
        else {
            return Array.from(this).mapSingle(function (el) {
                return el.innerHTML;
            });
        }
    }

    // Sets the class of an element or elements contained within a cesium object
    Ces.prototype.class = function (str, mode) {
        if(typeof str !== 'undefined') {
            if(typeof mode === 'undefined' || mode === true) {
                var className = '';
                if (typeof str !== 'string') {
                    for (var i = 0; i < str.length; i++) {
                        className += ' ' + str[i];
                    }
                } else {
                    className = ' ' + str;
                }
                return Array.from(this).forEach(function (el) {
                    el.className += className;
                });
            }
            else {
                return Array.from(this).forEach(function (el) {
                    var cs = el.className.split(' '), i;
            
                    while ( (i = cs.getPos(str)) > -1) { 
                        cs = cs.slice(0, i).concat(cs.slice(++i));
                    }
                    el.className = cs.join(' ');
                }); 
            }
        }
        else {
            return this.mapSingle(function (el) {
                if(el.getAttribute('class') !== null)
                    return el.getAttribute('class').split(' ');
                else
                    return null;
            });
        }
    }

    // Adds a new attribute to an element
    Ces.prototype.attribute = function (attr, val) {
        if (typeof val !== "undefined") {
            return Array.from(this).forEach(function(el) {
                el.setAttribute(attr, val);
            });
        } else {
            return this.mapSingle(function (el) {
                if(el.getAttribute('class') !== null)
                    return el.getAttribute('class').split(' ');
                else
                    return null;
            });
        }
    }

    // Creates new DOM element from html string
    Ces.prototype.new = function (str) {
        var div = document.createElement('div');
        div.innerHTML = str.trim();
        return div.childNodes;
    }

    // Appends a new element to the DOM
    Ces.prototype.append = function (el) {
        return Array.from(this).forEach(function (parent, i) {
            el.forEach(function (child) {
                if (i > 0) {
                    child = child.cloneNode(true); 
                }
                parent.appendChild(child);
            }); 
        }); 
    }

    // Prepends a new element to the DOM
    Ces.prototype.prepend = function (el) {
        return Array.from(this).forEach(function (parent, i) {
            for (var j = els.length -1; j > -1; j--) {
                child = (i > 0) ? els[j].cloneNode(true) : els[j];
                parent.insertBefore(child, parent.firstChild);
            }
        }); 
    }

    // Removes an element from the DOM
    Ces.prototype.remove = function () {
        return Array.from(this).forEach(function (el) {
            return el.parentNode.removeChild(el);
        });
    }

    // Hides an element - see .hide() function below
    Ces.prototype.hide = function (vis) {
        return Array.from(this).forEach(function (el) {
            return el.hide(vis);
        });
    }

    // Shows an element - see .show() function below
    Ces.prototype.show = function (dsp) {
        return Array.from(this).forEach(function (el) {
            return el.show(dsp);
        });
    }

    // Allows the user to define a new model
    Ces.prototype.DefineModel = function (obj) {
        return DefineModel(obj);
    }

    // Allows the user to define several new models at once
    Ces.prototype.DefineModels = function (arr) {
        if(arr.type().indexOf('Array') !== -1) {
            for(var i = 0; i < arr.length; i++) {
                DefineModel(arr[i]);
            }
        }
        else
            throw new TypeError('Parameter of unsupported type supplied for parameter "arr" of type array.');
    }

    // Allows the user to create a new instance of a model
    Ces.prototype.Model = function (str, obj) {
        return Model(str, obj);
    }

    // Build the Ces object
    var ces = {
        fn: function () {
            return new Ces;
        },
        get: function (selector) {
            //return element(selector);
            var el;
            if (typeof selector === 'string') {
                el = document.querySelectorAll(selector);
            } else if (selector.length) { 
                el = selector;
            } else {
                el = [selector];
            }
            return new Ces(el);
        },
        create: function (t, attrs) {
            var el = new Dome([document.createElement(t)]);
            if (attrs) {
                if (attrs.className) {
                    el.addClass(attrs.className);
                    delete attrs.className; // Remove now to avoid duplicates
                }
                if (attrs.text) {
                    el.text(attrs.text);
                    delete attrs.text;
                }
                for (var key in attrs) {
                    if (attrs.hasOwnProperty(key)) {
                        el.attr(key, attrs[key]);
                    }
                }
            }
            return el;
        },
        models: function () {
            return Models;
        }
    };
    return ces;
})()

// Allows the user to define a new model
function DefineModel(obj) {
    if(typeof obj === 'object') {
        if(obj.hasOwnProperty('name') && obj.hasOwnProperty('properties')) {
            if(typeof obj.properties === 'object') {
                Models.push(obj);
                return obj;
            }
            else
                throw new TypeError('Object of unsupported type supplied for property "properties" of type object.');
        }
        else
            throw new SyntaxError('Invalid model syntax.');
    }
    else
        throw new TypeError('Parameter of unsupported type supplied for parameter "obj" of type object.');
}

// Allows the user to create a new instance of a model
function Model(str, obj) {
    if(typeof str === 'string') {
        if(typeof obj === 'object' || obj === null || typeof obj === 'undefined') {
            // TODO: Do model stuff here
            var model = Models.filter(x => x.name === str)[0];
            var o = JSON.parse(JSON.stringify(model.properties));
            for (var prop in o) {
                if (o.hasOwnProperty(prop)) { // Filters out extra properties
                    // Initialise empty values for each model property
                    o[prop] = (o[prop].toLowerCase() === 'string' ? "" : (o[prop].toLowerCase() === 'object' ? {} : (o[prop].toLowerCase() === 'array' ? [] : (o[prop].toLowerCase() === 'integer' ? 0 : (o[prop].toLowerCase() === 'date' ? new Date() : (o[prop].toLowerCase() === 'null' ? null : (o[prop].toLowerCase() === 'undefined' ? undefined : undefined)))))));
                    // Set property value if values provided
                    if(arguments.length > 1) {
                        if(obj.hasOwnProperty(prop))
                            o[prop] = obj[prop];
                    }
                }
            }
            return o;
        }
        else
            throw new TypeError('Parameter of unsupported type supplied for optional parameter "obj" of type object.');
    }
    else
        throw new TypeError('Parameter of unsupported type supplied for parameter "str" of type string.');
}

/* Objects */

// Get the type of an object or test if it's equal to the value of a parameter either strictly or non-strictly based on  the bool 's', default non-strict
Object.prototype.type = function (t, s) {
    if(arguments.length === 0)
        return Object.prototype.toString.call(this);
    else if (t !== null && typeof t === 'string') {
        if(s !== null && typeof s === 'boolean') {
            if(s === true)
                return Object.prototype.toString.call(this).toLowerCase() === "[object " + t + "]";
            else if (s === false)
                return Object.prototype.toString.call(this).toLowerCase() == "[object " + t + "]";
            else
                throw new TypeError('Parameter of unsupported type supplied for optional parameter "s" of type bool.');
        }
        else
            return Object.prototype.toString.call(this).toLowerCase() == "[object " + t + "]";
    }
    else 
        throw new TypeError('Parameter of unsupported type supplied for optional parameter "t" of type string.');
}

// Compares two objects by converting them to JSON and then equating the JSON strings - not perfect but works for most applications
Object.prototype.compare = function (obj) {
    var thisJson = JSON.stringify(this);
    var objJson = JSON.stringify(obj);
    return thisJson === objJson;
}

/* Arrays */

// Get the first element of an array - not particularly useful but intuitive for new users
Array.prototype.first = function () {
    var arr = this;
    if (arr !== null && typeof arr !== 'undefined') {
        if (arr.length > 0)
            return arr[0];
        else {
            throw new TypeError('Cannot get the first element of an empty array.');
            return arr;
        }
    }
    else {
        throw new TypeError('Cannot get the first element of type ' + typeof arr + '.');
        return arr;
    }
}

// Get the last element of an array
Array.prototype.last = function () {
    var arr = this;
    if (arr !== null && typeof arr !== 'undefined') {
        if (arr.length > 0) {
            var end = arr.length;
            latest = arr[end - 1];
            return latest;
        }
        else {
            throw new TypeError('Cannot get the last element of an empty array.');
            return arr;
        }
    }
    else {
        throw new TypeError('Cannot get the last element of type ' + typeof arr + '.');
        return arr;
    }
}

// Iterates over an array and calls a function for each element
Array.prototype.each = function (fn) {
    if (this === null || typeof this === 'undefined')
        throw new TypeError('Cannot iterate over a collection of type null or undefined.');
    if(typeof fn !== 'function')
        throw new TypeError('Parameter of unsupported type supplied for parameter "fn" of type function.');

    var obj = new Object(this);
    var len = obj.length;
    var t;

    // Get the user defined argument if exists
    if(typeof arguments !== 'undefined') {
        if (arguments.length > 1)
            t = arguments[1];
    }

    for (i = 0; i < len; i++) {
        if (i in obj)
            fn.call(t, obj[i], i, obj); // (User Argument, Current Element, Index, Whole Object)
    }
}

// Select specific properties of an object or objects within an array
Array.prototype.select = function () {
    var arr = [];
    var args = [].slice.call(arguments);
    this.forEach(function (val,j){
        var obj = select(val, args);
        arr.push(obj);
    });
    return arr;
} 

function select(obj, arg) {
    var o = {};
    var keys = arg;
    for (var i=0; i<keys.length; i++) {
      if (keys[i] in obj) 
        o[keys[i]] = obj[keys[i]];
    }
    return o;
}

// Filter under the where name which is intuitive for those used to sql and linq
Array.prototype.where = function (fn) {
    var arr = this;
    return arr.filter(fn);
}

Array.prototype.contains = function (obj) {
    if(this.getPos(obj) !== -1)
        return true;
    else
        return false;
}

// Gets the index of an array that matches an object parameter - indexOf only does simple comparisons and findIndex isn't supported in IE
Array.prototype.getPos = function (o) {
    if(this !== null && this !== 'undefined') {
        var len = this.length;
        var obj = new Object(this);
        var len = obj.length;
        var t;

        // Get the user defined argument if exists
        if(typeof arguments !== 'undefined') {
            if (arguments.length > 1)
                t = arguments[1];
        }

        for (i = 0; i < len; i++) {
            if (i in obj)
                if(o.compare(obj[i])) // (User Argument, Current Element, Index, Whole Object)
                    return i;
        }
        return -1;
    }
    else
        throw new TypeError('Cannot get the position of an element in an empty array.');
}

/* Elements */

// Counts down in seconds, setting the text of the specified element to the countdown number
function counter($el, n) {
    (function loop() {
        $el.innerHTML = n ;
        n -= 1;
        if(n === -1)
            return;
        setTimeout(loop, 1000);
    })();
}

// Hides an element. Accepts a bool parameter that specifies if the element should be hidden with "visibility:hidden" or "display:none"
Element.prototype.hide = function (vis) {
    if (vis === null || typeof vis === 'undefined' || vis === false)
        this.style.display = 'none';
    else if (vis === true)
        this.style.visibility = 'hidden';
    else 
        throw new TypeError('Parameter of unsupported type supplied for optional parameter "vis" of type bool.');
}

// Shows an element. Accepts a string parameter that specifies which display value should be applied to the element (alternative to always applying "display:block" like jQuery)
Element.prototype.show = function (dsp) {
    if (this.style.visibility === 'hidden')
        this.style.visibility = 'visible';
    if(dsp === null || typeof dsp === 'undefined') {
        if (this.style.display = 'none')
            this.style.display = 'block';
    }
    else if(typeof dsp === 'string')
        this.style.display = dsp;
    else
        throw new TypeError('Parameter of unsupported type supplied for optional parameter "dsp" of type string.');
}

// Gets an element by id, class or selector - multiple matching elements for a class are placed in an array
function element(s) {
    if(arguments.length === 0)
        throw new TypeError('No parameter specified. The "element" function requires either an Id, Class or Selector as a parameter.');
    else if (typeof s === 'string') {
        if(document.querySelectorAll(s).length > 1)
            return document.querySelectorAll(s);
        else
            return document.querySelector(s);
    }
    else if (s.length)
        return s;
    else
        return [s];
}

/* Events */

// Add ready event listener to document
Document.prototype.ready = function (fn) {
    (function () {
        this.addEventListener( "load", function () {
            fn.call();
        });
    })()
}

// Add ready event listener to window
Window.prototype.ready = function (fn) {
    (function () {
        this.addEventListener( "load", function () {
            fn.call();
        });
    })()
}

// Runs callback function on ready - "load" used for browser support up to IE10
function ready(fn) {
    (function () {
        window.addEventListener( "load", function () {
            fn.call();
        });
    })()
}

/* Misc */

// Waits for a certain length of time and then executes callback like setTimeout()
function wait(fn, ms) {
    if(ms === null || !intParse(ms)) {
        if(typeof fn === 'function') {
            var Wait = ms => new Promise((r, j)=>setTimeout(r, ms));
            Wait.then(function () {
                fn();
            });
        }
        else
            throw new TypeError(fn + 'is not a function. wait() requires a parameter of type function.');
    }
    else
        throw new TypeError('Incorrect parameter type. wait() requires time interval parameter of type int.');
}

// Returns true if a JSON string is valid
String.prototype.JSONValidate = function () {
    try {
        JSON.parse(this);
    }
    catch {
        return false;
    }
    return true;
}

// Returns true if a string contains a substring that matches a string parameter
String.prototype.contains = function (str) {
    if (typeof str === 'string') {
        if(this.indexOf(str) !== -1)
            return true;
        else
            return false;
    }
    else
        throw new TypeError('Incorrect parameter type. .contains() requires parameter of type string.');
}